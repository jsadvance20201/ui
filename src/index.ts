import { Button } from './components/button';
import { TextInput, TextArea } from './components/Inputs';
import { OrganizationInfo } from './components/organizationInfo';
import { Typography } from './components/typography';
import { PersonVertical, PersonHorizontal } from './components/person';
import { PageLoader } from './components/page-loader';
import { ImgLink } from './components/img-link';
import { Footer } from './components/footer';
import { ReachGoals } from './components/reach-goals';
import { Navbar } from './components/navbar';
import Error from './components/error';
import { SocialShare } from './components/social-share';
import {ModalInfo} from './components/modal'

export {
    Button,
    OrganizationInfo,
    Typography,
    PersonVertical,
    PersonHorizontal,
    PageLoader,
    TextInput,
    TextArea,
    ImgLink,
    Footer,
    ReachGoals,
    Navbar,
    Error,
    SocialShare,
    ModalInfo
};
