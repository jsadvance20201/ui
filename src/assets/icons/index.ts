import avaFemale from './ava-fem.svg';
import avaMale from './ava-mal.svg';
import udsIcon from './uds-info.svg';
import logo from './logo.svg';
import logoGray from './logo-gray.svg';
import phoneNumber from './number.svg';
import phoneNumberGray from './numberGray.svg';
import menuOpen from './menu-open.svg';
import menuClosed from './menu-closed.svg';
import vkLogo from './VK-logo.svg';
import twitterLogo from './Twitter-logo.svg';
import okLogo from './Odnoklassniki-logo.svg';
import instagramLogo from './Instagram-logo.svg';
import facebookLogo from './Facebook-logo.svg';
import icoSuccess from './ico-success.svg'
import icoUnsuccess from './ico-unsuccess.svg'

// Social nets icons
import vkIcon from './vk.svg';
import okIcon from './ok.svg';
import twitterIcon from './twitter.svg';
import instagramIcon from './instagram.svg';
import facebookIcon from './facebook.svg';

export {
    avaMale,
    avaFemale,
    udsIcon,
    logo,
    logoGray,
    phoneNumber,
    phoneNumberGray,
    vkIcon,
    okIcon,
    twitterIcon,
    instagramIcon,
    facebookIcon,
    menuOpen,
    menuClosed,
    vkLogo,
    twitterLogo,
    okLogo,
    instagramLogo,
    facebookLogo,
    icoSuccess,
    icoUnsuccess
};
