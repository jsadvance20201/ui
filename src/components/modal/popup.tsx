import { Typography } from '../../components/typography';
import { Button } from '../../components/button';
import {PopupModal, CloseButton} from './styled';
import  React, {useEffect, useState} from 'react';
import {icoSuccess, icoUnsuccess} from '../../assets'

/**
 * @prop {boolean} appear Открыть/закрыть модальное окно
 * @prop {function} setPopup Обрабтчик закрытия окна
 * @prop {string} image название картинки (ico-success/ico-unsuccess)
 * @prop {title} Заголовок
 * @prop {body} Основной текст
 */
interface PopupProps {
  appear: boolean;
  setPopup: (string) => void;
  image: string;
  title: string;
  body: string;
}

const imageSet = {
  'ico-success': icoSuccess,
  'ico-unsuccess': icoUnsuccess
}
/**
 * Компонент, использующийся для уведомления о успешном или неудачном заполнении формы в модальном окне.
 * Состоит из:
 *  Изображение
 *  Заголовок
 *  Текст 
 *  Кнопка 'Понятно'
 */

export const Popup: React.FC<PopupProps> =(props)=> {
  const [isOpen, setIsOpen] = useState(props.appear);
  useEffect(() => setIsOpen(props.appear), [props.appear]);

  const toggleModal: any = (e) => { 
    setIsOpen(!isOpen)
  }

  return (
    <PopupModal
    isOpen={isOpen}
    onBackgroundClick={() => props.setPopup('')}
    onEscapeKeydown={() => props.setPopup('')}>
      <CloseButton onClick={() => props.setPopup('')}>
          <img src="https://icon.now.sh/x" />
      </CloseButton>
      <img src={imageSet[props.image]}/>
      <Typography 
        component='h2'
        fontSize='38px'
        fontWeight={600}>
          {props.title}
      </Typography>
      <Typography
      style={{textAlign: 'center'}}
      component='p'
      fontSize='16px'
      >
        {props.body}
      </Typography>
      <Button onClick={() => props.setPopup('')}>Понятно</Button>
    </PopupModal>
  )
}