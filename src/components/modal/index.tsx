import * as React from 'react';
import {StyledModal, CloseButton, Label, StyledSelect, FullText, DivLeft, DivRight} from './styled'
import { Typography } from '../../components/typography'
import { Button } from '../../components/button';

import { TextInput } from '../../components/Inputs';

type PopupData = {
  id: number; 
  section: string
}

/**
 * @prop {string} label Название(лейбл) 
 * @prop {array} data Массив состоящий из объектов со свойствами id и section. Каждый объект это значение в раскрывающемся списке
 */
interface SelectProps {
  label: string;
  data: PopupData[]
}
/**
 * Компонент - инпут в виде раскрывающегося списка
 */
const Select: React.FC<SelectProps> = (selProps) => {
  // if (!selProps.data?.length){
  //   return <PageLoader />
  // }
  return (
    <div>
      <Label required>
        {selProps.label}
      </Label>
      <StyledSelect>
        {selProps.data.map((item) => {
        return (
          <option style={{width:'100%'}} key={item.id}>
            {item.section}
          </option>
        )
        })}
      </StyledSelect>
    </div>
  )
}


interface ModalProps {
  showModal: boolean;
  toggleModal: () => void;
  setPopup: (string) => void;
  region: PopupData[];
  institution: PopupData[];
  sections: PopupData[];
}




export const ModalInfo: React.FC<ModalProps> = (props) => {
  const inputStyle = {
    marginBottom: 20
  }

  return (
    <StyledModal
    isOpen={props.showModal}
    onBackgroundClick={props.toggleModal}
    onEscapeKeydown={props.toggleModal}
    >
      <CloseButton onClick={props.toggleModal}>
          <img src="https://icon.now.sh/x" />
      </CloseButton>
      <DivLeft>
        <FullText><i>UDS</i></FullText>
      </DivLeft>
      <DivRight> 
        <form>
          <Typography
            component={'h2'}
            fontWeight={600}
          >
            Запись
          </Typography>
          <Select label='Округ или район' data={props.region} />
          <Select label='Учреждение' data={props.institution} />
          <Select label='Секция' data={props.sections} />
          <TextInput style={inputStyle} label='Имя' required />
          <TextInput style={inputStyle} label='Фамилия' required/>
          <TextInput style={inputStyle} label='Телефон, по которому мы можем с вами связаться' type='tel' required/>
          <TextInput style={inputStyle} label='Электронная почта'/>
          <TextInput style={inputStyle} label='Комментарий' />
          <Button style={{margin: '15px 0'}}>Записаться</Button>
        </form>
      </DivRight> 
    </StyledModal>
  )
}