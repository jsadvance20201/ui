import styled from 'styled-components';
import Modal from 'styled-react-modal';
import { COLORS } from '../../constants/colors';

export const StyledModal = Modal.styled`
  position: fixed;
  width: 47rem;
  display: flex;
  align-items: center;
  justify-content: center;

  flex-direction: row;
  min-width: 320px;
  background: white;
  border-radius: 20px;
  box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.3);
  `

  export const CloseButton = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  border: none;
  background: transparent;
  padding: 15px;
  &:hover {    
    cursor: pointer;
  }
`;

export const Label = styled.label`
  font-size: 10px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: rgb(142,142,143);
  text-transform: uppercase;
  
  &:after {
    color: ${COLORS.accent};
    content: ${(props: { required?: boolean}) => props.required ? '" *"' : ''};
  }
  `;

  export const StyledSelect = styled.select`
  width: 100%;
  height: 40px;
  border: 1px solid rgb(206, 208, 218);
  border-radius: 4px;
  color: #a6abb2;
  margin-top: 5px;
  padding: 0 5px;
  font-size: 14px;
  background-color: #ffffff;

  &:focus {
    outline: none;
  }
`;

export const FullText = styled.p`
  transform: rotate(270deg);
  font-family: 'Arial Black'; 
  font-size: 350px;
  color: #4E51D8; 
  margin-top: 470px;
`;

export const DivRight = styled.div`
  width: 65%;
  padding: 20px;  
`;


export const DivLeft = styled.div`
  max-height: 48rem;
  border-radius: 20px 0px 0px 20px;
  background-clip: padding-box;
  overflow: hidden;
  width: 30%;
  background: #1A1EB2;
  align-self: stretch;
  text-align: center;
  vertical-align: center;
`;

export const PopupModal = Modal.styled`
  width: 400px;
  height: 310px;
  position: fixed;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 45px;

  min-width: 320px;
  background: white;
  border-radius: 20px;
  box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.3);
  `;