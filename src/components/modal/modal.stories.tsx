import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import {ModalProvider} from 'styled-react-modal';
import { ModalInfo } from '.';
import {Popup} from './popup';
import { Button } from '../button';



// const [isOpen, setIsOpen] = React.useState(false);
// const [popup, setPopup] = React.useState('');

// const toggleModal: any = (e) => {
//     setIsOpen(!isOpen)
// }
  
// const popupToShow: any = (type: string) => {
//     setPopup(type)
// }
const dataSection = [
  {id: 0, section: 'Секция 1'},
  {id: 1, section: 'Секция 2'},
  {id: 2, section: 'Секция 3'},
]
storiesOf('ModalInfo', module)

    .add('Modal', () => 
    <ModalProvider>
      <ModalInfo 
        showModal={true} 
        toggleModal={action('button-click')} 
        setPopup={action('button-click')}
        region={dataSection}
        institution={dataSection}
        sections={dataSection} />
    </ModalProvider>
    )
    .add('PopupSuccess', () => 
    <ModalProvider>
      <Popup 
        appear={true}
        image={'ico-success'}
        title={'Здорово!'}
        body={'Наш менеджер свяжется с вами по указанному телефону'} 
        setPopup={action('button-click')}/>
    </ModalProvider>
    )
    .add('PopupUnsuccess', () => 
    <ModalProvider>
      <Popup 
        appear={ true }
        image={'ico-unsuccess'}
        title={'Ой!'}
        body={`Что-то пошло не так. Повторите попытку позже.`} 
        setPopup={action('button-click')}/>
    </ModalProvider>
    )
    