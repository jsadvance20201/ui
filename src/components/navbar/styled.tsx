import styled from 'styled-components'

const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: row wrap;
    flex-flow: row wrap;
`

export const Main = styled(Wrapper)`
    align-content: center;
    justify-content: space-between;
`

export const LeftSection = styled(Wrapper)`
    flex: 0 0 20%;
    img {
        height: 32px;
    }

    @media (min-width: 1000px) {
        flex: 0 0 25%;
    }

    @media (min-width: 810px) {
        img {
            height: 48px;
        }
    }

`

export const RightSection = styled(Wrapper)`
    flex: 0 0 20%;
    align-items: center;
    justify-content: flex-end;
    text-align: center;

    @media (min-width: 1000px) {
        flex: 1 0 60%;
    }

    @media (min-width: 1250px) {
        flex: 1 0 70%;
    }
`

export const Link = styled.a`
    display: flex;
    padding: 0;
    text-decoration: none;
    margin-bottom: 33px;

    @media (min-width: 1000px) {
        display: block;
        padding: 15px;
        margin-bottom: 0;
    }
`

export const ActiveLink = styled(Link)`
    display: none;

    span {
        border-bottom: 3px #003EFF solid;
    }

    @media (min-width: 810px) {
        display: flex;
    }
`

export const Links = styled(Wrapper)`
    display: ${props => props.isOpen ? 'flex' : 'none'};
    margin-top: ${props => props.isOpen ? '64px' : '0'};
    -webkit-flex-flow: column wrap;
    flex-flow: column wrap;
    flex: 0 0 100%;

    @media (min-width: 1001px) {
        display: flex;
        justify-content: flex-end;
        -webkit-flex-flow: row wrap;
        flex-flow: row wrap;
    }
`

export const PhoneIcon = styled.img`
    display: ${props => props.isOpen ? 'flex' : 'none'};
    margin-top: 24px;

    @media (min-width: 1200px) {
        display: flex;
        flex: 0 0 20%;
        margin-top: 0;
    }
`

export const MenuIcon = styled.img`
    display: flex;
    &: hover {
        cursor: pointer;
        filter: brightness(1.5);
    }
    
    @media (min-width: 1001px) {
        display: none;
    }
`