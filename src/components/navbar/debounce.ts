const debounce = (fn: Function, ms: number) => {
    let timer;
    return () => {
        clearTimeout(timer);
        timer = setTimeout(function () {
            timer = null;
            fn.apply(this, arguments);
        },                 ms);
    };
};

export default debounce;