import React from 'react'

import { Typography } from '../typography'
import { Link, Links as Wrapper, ActiveLink } from './styled'
import { linkTypes, linkTitles } from './constants'
import type { LinkInfo } from './constants'

/**
 * @prop {LinkInfo} hrefs Object of links hyper links.
 * @prop {boolean} isOpen Should content be viewed on small screen
 * @prop {'main' | 'orgs' | 'news' | 'sections' | 'rates'} active Active link identifier. Optional parameter
 * @prop {LinkInfo} titles  Object of links texts.
 */
interface LinkProps {
    hrefs: LinkInfo;
    isOpen: boolean;
    active?: 'main' | 'orgs' | 'news' | 'sections' | 'rates';
    titles?: LinkInfo;
}

/**
 * Компонент использующийся для отображения ссылок в NavBar
 * 
 * Wrapper:
 *      Ссылка:
 *          Текст
 *      Ссылка:
 *          Текст
 *      Переданные дети
 */
export const Links: React.FC<LinkProps> = ({ titles, hrefs, active, isOpen, children }) => {
    titles = titles || linkTitles

    // Create Link and ActiveLink components based on incoming links
    const links = linkTypes.map(type => {
        if (!titles[type] || !hrefs[type])
            return

        const hoverStyle = `
            &: hover {
                filter: brightness(1.5);
            }
        `

        const title = (
            <Typography
                component='span'
                fontSize='14px'
                fontWeight={700}
                textTransform="uppercase"
                color="#003EFF"
                customStyle={hoverStyle}
            >
                {titles[type]}
            </Typography>
        )

        if (active === type) {
            return <ActiveLink href={hrefs[type]} key={hrefs[type]}>{title}</ActiveLink>
        }

        return <Link href={hrefs[type]} key={hrefs[type]}> {title}</Link>
    })

    return (
        <Wrapper isOpen={isOpen}>
            {links}
            {children}
        </Wrapper>
    )
}