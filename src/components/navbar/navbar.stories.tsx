import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { Navbar } from '.';

storiesOf('Navbar', module)
    .add('default', () => (
        <Navbar
            main='/main'
            orgs='/orgs'
            news='/news'
            sections='/sections'
            rates='/rates'
            active='main'
            delay={250}
        />
    ))
    .add('empty', () => (
        <Navbar />
    ))


