import React, { useState, useEffect } from 'react'

import {
    Main,
    RightSection,
    LeftSection,
    MenuIcon,
    PhoneIcon
} from './styled'

import {
    logo,
    phoneNumber,
    menuOpen,
    menuClosed
} from '../../assets'

import { ImgLink } from '../img-link'
import { Typography } from '../typography'
import { Links } from './Links'
import { LinkInfo } from './constants'
import debounce from './debounce'


/**
 * @prop {string} main Hyper link to the main page. Optional parameter
 * @prop {string} orgs Hyper link to the orgs page. Optional parameter
 * @prop {string} news Hyper link to the news page. Optional parameter
 * @prop {string} sections Hyper link to the sections page. Optional parameter
 * @prop {string} rates Hyper link to the rates page. Optional parameter
 * @prop {string} active The current active link. Optional parameter
 * @prop {LinkInfo} titles Override default value of each link's title. Optional parameter 
 * @prop {number} delay Delay between screen resize checks. The less value the less performance, but higher fluidity. Optional parameter
 */
interface NavbarProps {
    main?: string;
    orgs?: string;
    news?: string;
    sections?: string;
    rates?: string;
    active?: 'main' | 'orgs' | 'news' | 'sections' | 'rates';
    titles?: LinkInfo;
    delay?: number;
}

/**
 * Компонент использующийся наверху страницы
 * 
 * NavBar:
 *      Левая секция:
 *          Иконка или текст
 *      Правая секция:
 *          Ссылки или иконка 
 */
export const Navbar: React.FC<NavbarProps> = ({ main, orgs, news, sections, rates, active, delay }) => {
    const [isOpen, setIsOpen] = useState(false)

    // Listener of screen resize
    useEffect(() => {
        const debouncedHandleResize = debounce(function handleResize() {
            setIsOpen(window.innerWidth > 1000 ? false : isOpen)
        }, delay || 1000)

        window.addEventListener('resize', debouncedHandleResize)

        return () => {
            window.removeEventListener('resize', debouncedHandleResize)
        }
    })

    const currentMenuIcon = isOpen ? menuClosed : menuOpen

    const links = (
        <Links
            isOpen={isOpen}
            hrefs={{
                main: main,
                orgs: orgs,
                news: news,
                sections: sections,
                rates: rates
            }}
            active={active}
        >
            <span style={{ display: 'flex' }}>
                <PhoneIcon
                    src={phoneNumber}
                    isOpen={isOpen}
                />
            </span>
        </Links>
    )

    return (
        <Main>
            <LeftSection>
                {isOpen && <Typography component='h1' fontSize='24px' fontWeight={600}> Меню </Typography>}
                {!isOpen && <ImgLink src={logo} href='/' stay />}
            </LeftSection>
            <RightSection>
                {!isOpen && links}
                <MenuIcon src={currentMenuIcon} onClick={() => setIsOpen(!isOpen)} />
            </RightSection>
            {isOpen && links}
        </Main>
    )
}