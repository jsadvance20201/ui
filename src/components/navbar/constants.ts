export const linkTypes: string[] = ['main', 'orgs', 'news', 'sections', 'rates'];

export const linkTitles = {
    main: 'Главная',
    orgs: 'Учреждения',
    news: 'Новости',
    sections: 'Секции',
    rates: 'Рейтинги'
};

export interface LinkInfo {
    main?: string;
    orgs?: string;
    news?: string;
    sections?: string;
    rates?: string;
}
