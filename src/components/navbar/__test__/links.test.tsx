import * as React from 'react';
import renderer from 'react-test-renderer';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { Links } from '../Links';

describe('Links component', () => {
    it('should match snapshot', () => {
        const tree = mount(
            <Links
                isOpen={false}
                hrefs={{
                    main: '/main',
                    orgs: '/orgs',
                    news: '/news',
                    sections: '/sections',
                    rates: '/rates'
                }}
                active='news'
            />
        )

        expect(tree).toMatchSnapshot()
    })

    it('should draw only links that present in hrefs and titles', () => {
        const tree = mount(
            <Links
                isOpen={false}
                hrefs={{
                    main: '/main',
                    orgs: '/orgs',
                    news: '/news',
                    sections: '/sections',
                }}
                active='news'
            />
        )

        expect(tree.find('a').length).toBe(4)
    })
})