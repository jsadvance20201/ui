import * as React from 'react';
import renderer from 'react-test-renderer';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { Navbar } from '..';

describe('Navbar component', () => {
    const tree = mount(
        <Navbar
            main='/main'
            orgs='/orgs'
            news='/news'
            sections='/sections'
            rates='/rates'
            active='main'
            delay={250}
        />
    );

    it('should match snapshot', () => {
        expect(tree).toMatchSnapshot();
    });

    it('should match snapshot on small screen', () => {
        // @ts-ignore
        window.innerWidth = 800
        window.dispatchEvent(new Event('resize'))

        expect(tree).toMatchSnapshot();
    })

    it('should draw less links by one on small screen after open collapse', () => {
        const aNumber = tree.find('a').length

        // @ts-ignore
        window.innerWidth = 800
        window.dispatchEvent(new Event('resize'))

        // Click to collapse icon
        tree.find('img').last().simulate('click')

        expect(tree.find('a').length).toBe(aNumber - 1)
    })

    it('should change icon to h1 by clicking on collapse icon', () => {
        // @ts-ignore
        window.innerWidth = 800
        window.dispatchEvent(new Event('resize'))

        // Click to collapse icon
        tree.find('img').last().simulate('click')

        expect(tree.find('h1').length).toBe(1)

        // Click to collapse icon
        tree.find('img').last().simulate('click')

        expect(tree.find('a').first().find('a').length).toBe(1)
    })

    it('should make isOpen state to false on big screens after 250ms', () => {
        const aNumber = tree.find('a').length

        // @ts-ignore
        window.innerWidth = 800
        window.dispatchEvent(new Event('resize'))

        // Click to collapse icon
        tree.find('img').last().simulate('click')

        // @ts-ignore
        window.innerWidth = 1400
        window.dispatchEvent(new Event('resize'))

        setTimeout(() => {
            expect(tree.find('a').first().find('img')).toBe(1)
            expect(tree.find('a').length).toBe(aNumber)
        }, 250)
    })

    it('should set default delay to 1000 if it is not present', () => {
        const tree = mount(
            <Navbar
                main='/main'
                orgs='/orgs'
                news='/news'
                sections='/sections'
                rates='/rates'
                active='main'
            />
        );

        const aNumber = tree.find('a').length

        // @ts-ignore
        window.innerWidth = 800
        window.dispatchEvent(new Event('resize'))

        // Click to collapse icon
        tree.find('img').last().simulate('click')

        // @ts-ignore
        window.innerWidth = 1400
        window.dispatchEvent(new Event('resize'))

        setTimeout(() => {
            expect(tree.find('a').first().find('img')).toBe(1)
            expect(tree.find('a').length).toBe(aNumber)
        }, 250)
    })
});

