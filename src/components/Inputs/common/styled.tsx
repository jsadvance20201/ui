import styled from 'styled-components';

import { COLORS } from '../../../constants/colors';

export const Wrapper = styled.div`
  text-align: left;
  display: flex;
  flex-direction: column;
`;

export const Label = styled.label`
  font-size: 10px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  color: rgb(142,142,143);
  text-transform: uppercase;

  &:after {
    color: ${COLORS.accent};
    content: ${(props: { required?: boolean}) => props.required ? '" *"' : ''};
  }
`;

export const StyledInput = styled.input`
  width: calc(100% - 30px);
  height: 40px;
  border: 1px solid rgb(206, 208, 218);
  border-radius: 4px;
  color: #a6abb2;
  margin-top: 5px;
  padding: 0 15px;
  font-size: 14px;
  background-color: #ffffff;

  &:focus {
    outline: none;
  }
`;

export const StyledTextArea = styled.textarea`
  width: calc(100% - 30px);
  padding: 15px;
  border: 1px solid rgb(206, 208, 218);
  border-radius: 4px;
  color: #a6abb2;
  margin-top: 5px;
  font-size: 14px;
  min-width: 230px;
  background-color: #fff;

  &:focus {
    outline: none;
  }
`;
