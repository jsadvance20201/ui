import TextInput from './TextInput/input';
import TextArea from './TextArea/text-area';

export {
    TextArea,
    TextInput
};
