import * as React from 'react';

import { avaMale, avaFemale } from '../../assets';

import {
    PersonContainerColumn,
    PersonContainerRow,
    PersonAvatarContainer,
    CenterText,
    PersonColumnDescriptionContainer,
    PersonImg
} from './styled';

import { Typography } from '../typography'

interface PersonProps {
    name: string;
    jobTitle: string;
    gender: 'female' | 'male';
    image?: string;
    jobDescription?: string;
}

const DefaultImage = {
    female: avaFemale,
    male: avaMale
};

export const PersonVertical: React.FunctionComponent<PersonProps> = ({ image, name, jobTitle, gender }) => (
    <PersonContainerColumn>
        <PersonAvatarContainer>
            <PersonImg
                src={image || DefaultImage[gender]}
            />
        </PersonAvatarContainer>
        <CenterText>
            <Typography
                component="p"
                fontSize="22px"
                fontWeight={600}
                margin="0 0 8px 0"
            >
                {name}
            </Typography>
            <Typography
                component="p"
                fontSize="16px"
                fontWeight={400}
            >
                {jobTitle}
            </Typography>
        </CenterText>
    </PersonContainerColumn>
)

export const PersonHorizontal: React.FunctionComponent<PersonProps> = ({ image, name, jobTitle, jobDescription, gender }) => (
    <PersonContainerRow>
        <PersonAvatarContainer>
            <PersonImg
                src={image || DefaultImage[gender]}
            />
        </PersonAvatarContainer>
        <PersonColumnDescriptionContainer>
            <Typography
                component="p"
                fontSize="22px"
                fontWeight={600}
                margin="0 0 8px 0"
            >
                {name}
            </Typography>
            <Typography
                component="p"
                fontSize="16px"
                fontWeight={700}
                margin="0 0 24px 0"
            >
                {jobTitle}
            </Typography>
            <Typography
                component="p"
                fontSize="16px"
                fontWeight={400}
            >
                {jobDescription}
            </Typography>
        </PersonColumnDescriptionContainer>
    </PersonContainerRow>
);