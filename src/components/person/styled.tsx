import styled from 'styled-components';

export const PersonContainerColumn = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    max-width: 300px;
`;

export const PersonContainerRow = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    max-width: 900px;

    flex-direction: column;
    @media (min-width: 810px) {
        flex-direction: row;
        align-items: flex-start;
    }
`;

export const PersonAvatarContainer = styled.div`
    height: 168px;
    width: 168px;
    margin-bottom: 13px;
    flex-shrink: 0;
`;

export const CenterText = styled.span`
    text-align: center;
`;

export const PersonColumnDescriptionContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    margin-left: 20px;
`;

export const PersonImg = styled.img`
    height: 100%;
    width: 100%;
    border-radius: 50%;
`;