import React from 'react'

import { LinkWrapper } from './styled'
import { ImgLink } from '../img-link'

interface LinkProps {
    src: string;
    href?: string;
}


const Link: React.FC<LinkProps> = ({ src, href }) => (
    <LinkWrapper>
        <ImgLink src={src} href={href} />
    </LinkWrapper>
)

export default Link
