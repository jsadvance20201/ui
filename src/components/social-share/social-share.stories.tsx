import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { SocialShare } from '.';

storiesOf('Social Share', module)
    .add('default share with default links', () => (
        <SocialShare title={'Мы в социальных сетях'} />
    ))
    .add('share with partially provided links links', () => (
        <SocialShare
            title={'Мы в социальных сетях'}
            vk_link={'https://vk.com'}
            twitter_link={'https://twitter.com'}

        />
    ))
    .add('share with provided links links', () => (
        <SocialShare
            title={'Мы в социальных сетях'}
            vk_link={'https://vk.com'}
            twitter_link={'https://twitter.com'}
            ok_link={'https://ok.ru'}
            instagram_link={'https://instagram.com'}
            facebook_link={'https://facebook.com'}
        />
    ))
    .add('alternative text on small screens', () => (
        <SocialShare
            title={'Мы в социальных сетях'}
            alt={'Мы в соцсетях'}
            vk_link={'https://vk.com'}
            twitter_link={'https://twitter.com'}
            ok_link={'https://ok.ru'}
            instagram_link={'https://instagram.com'}
            facebook_link={'https://facebook.com'}
        />
    ));