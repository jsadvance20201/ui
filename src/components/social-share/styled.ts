import styled from 'styled-components';

export const Wrapper = styled.div`
    text-align: center;
    font-family: Helvetica;
    font-size: 24px;
    font-weight: 600;
`;

export const LinkWrapper = styled.div`
    display: inline-block;

    margin: 0 12px;
    img {
        height: 32px;
    }

    @media (min-width: 810px) {
        margin: 0 24px;
        img {
            height: 64px;
        }
    }
`

export const TitleStyle = (title, alt) => `
    font-size: 22px;
    line-height: 26px; 
    margin-bottom: 16px;
    
    &:before {
        content: "${alt || title}";
    }

    @media (min-width: 810px) {
        &:before {
            content: "${title}";
        }

        font-size: 38px;
        line-height: 46px;
        margin-bottom: 24px;
    } 
`