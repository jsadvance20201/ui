import React from 'react';

import Link from './Link';
import { Typography } from '../typography'
import { vkLogo, twitterLogo, okLogo, instagramLogo, facebookLogo } from '../../assets';

import { Wrapper, TitleStyle } from './styled';

/**
 * @prop {string} title The title of a block
 * @prop {string} alt The title in a small screens
 * @prop {string} vk_link The link of a vk logo
 * @prop {string} twitter_link The link of a twitter logo
 * @prop {string} ok_link The link of a ok logo
 * @prop {string} instagram_link The link of a instagram logo
 * @prop {string} facebook_link The link of a facebook logo
 */
interface SocialSharesProps {
    title: string;
    alt?: string;
    vk_link?: string;
    twitter_link?: string;
    ok_link?: string;
    instagram_link?: string;
    facebook_link?: string;
}

export const SocialShare: React.FC<SocialSharesProps> = (props) => {
    return (
        <Wrapper>
            <Typography
                component='p'
                fontWeight={600}
                customStyle={TitleStyle(props.title, props.alt)}
            />
            <Link src={vkLogo} href={props.vk_link} />
            <Link src={twitterLogo} href={props.twitter_link} />
            <Link src={okLogo} href={props.ok_link} />
            <Link src={instagramLogo} href={props.instagram_link} />
            <Link src={facebookLogo} href={props.facebook_link} />
        </Wrapper>
    );
};

SocialShare.defaultProps = {
    vk_link: '#',
    twitter_link: '#',
    ok_link: '#',
    instagram_link: '#',
    facebook_link: '#',
};
