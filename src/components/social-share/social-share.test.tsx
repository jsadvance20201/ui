import * as React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

import { SocialShare } from '.';

describe('ImageLink component', () => {
    it('basic rendering', () => {
        expect(mount(<SocialShare title={'Мы в социальных сетях'}/>)).toMatchSnapshot();
        expect(mount(
            <SocialShare
                title={'Мы в социальных сетях'}
                vk_link={'https://vk.com'}
                twitter_link={'https://twitter.com'}
            />
        )).toMatchSnapshot();
        expect(mount(
            <SocialShare
                title={'Мы в социальных сетях'}
                vk_link={'https://vk.com'}
                twitter_link={'https://twitter.com'}
                ok_link={'https://ok.ru'}
                instagram_link={'https://instagram.com'}
                facebook_link={'https://facebook.com'}
            />
        )).toMatchSnapshot();
    });
});
