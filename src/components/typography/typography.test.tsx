import * as React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { Typography } from './';

describe('Typography component', () => {
  it('Отрисовка стандартного компонента', () => {
    const typography = mount(<Typography />);

    expect(typography).toMatchSnapshot();
  });

  it('Html компонент заменяется на указанный в props.component', () => {
    const customComponentTypography = renderer.create(<Typography component="h3" />).toJSON();

    expect(customComponentTypography).toHaveProperty('type', 'h3');
    expect(customComponentTypography).toMatchSnapshot();
  });

  it('Html компонент заменяется на указанный в таблице variant', () => {
    const customComponentTypography = renderer.create(<Typography variant="description" />).toJSON();

    expect(customComponentTypography).toHaveProperty('type', 'span');
    expect(customComponentTypography).toMatchSnapshot();
  });

  it('Принимает параметры для стилизации', () => {
    const styledColorTypography = mount(<Typography color="#fefefe" />);
    const styledMarginTypography = mount(<Typography margin="10px" />);
    const styledPaddingTypography = mount(<Typography padding="10px" />);
    const styledFontSizeTypography = mount(<Typography fontSize="10px" />);
    const styledTextTransformTypography = mount(<Typography textTransform="uppercase" />);
    const styledFontWeightTypography = mount(<Typography fontWeight={700} />);
    const styledOpacityTypography = mount(<Typography opacity="0.3" />);

    expect(styledColorTypography).toMatchSnapshot();
    expect(styledMarginTypography).toMatchSnapshot();
    expect(styledPaddingTypography).toMatchSnapshot();
    expect(styledFontSizeTypography).toMatchSnapshot();
    expect(styledTextTransformTypography).toMatchSnapshot();
    expect(styledFontWeightTypography).toMatchSnapshot();
    expect(styledOpacityTypography).toMatchSnapshot();
  });

  it('Захватывает стили для опреденных компонентов, предустановленных вариаций', () => {
    const h1Typography = mount(<Typography component="h1" />);
    const h2Typography = mount(<Typography component="h2" />);
    const h3Typography = mount(<Typography component="h3" />);
    const pTypography = mount(<Typography component="p" />);
    const primaryTypography = mount(<Typography variant="primary" />);
    const secondaryTypography = mount(<Typography variant="description" />);

    expect(h1Typography).toMatchSnapshot();
    expect(h2Typography).toMatchSnapshot();
    expect(h3Typography).toMatchSnapshot();
    expect(pTypography).toMatchSnapshot();
    expect(primaryTypography).toMatchSnapshot();
    expect(secondaryTypography).toMatchSnapshot();
  });

  it('Кнопка как typography', () => {
    const jestCallback = jest.fn();
    const btnTypography = shallow(<Typography component={'button'} onClick={jestCallback} />);

    btnTypography.simulate('click');
    btnTypography.simulate('click');

    expect(jestCallback.mock.calls.length).toBe(2);
  });
});
