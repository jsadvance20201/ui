import * as React from 'react';

import {
    Wrapper,
    LeftSection,
    RightSection,
    Icon,
} from './styled'

import { Button } from '../../components/button';
import { Typography } from '../../components/typography'

interface OnClickType {
    (event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void;
}

/**
 * @prop {string} title Заголовок.
 * @prop {string} mainText Основной текст.
 * @prop {string} bottomText жирный текст под [bottomText].
 * @prop {string} buttonText текст кнопки, что находиться под [buttonText].
 * @prop {string} src ссылка на картинку.
 * @prop {function} onClick Обработчки нажатия на кнопку.
 */
interface OrganizationInfoProps {
    title: string;
    mainText: string;
    bottomText: string;
    buttonText: string;
    src: string;
    onClick?: OnClickType;
}

/**
 * Компонент, как правило, использующийся, чтобы отображать информацию про учреждение
 * Состоит из двух чатей:
 *     Левая часть (колонкой):
 *         заголовок
 *         текст
 *         жирный текст
 *         кнопка
 *     Правая часть: изображение
 */
export const OrganizationInfo: React.FC<OrganizationInfoProps> = ({ title, mainText, bottomText, buttonText, src, onClick }) => {
    const titleFontSize = `
        font-size: 20px;
        @media (min-width: 810px) {
            font-size: 50px;
        } 
    `

    return (
        <Wrapper>
            <LeftSection>
                <Typography
                    component={'h1'}
                    fontWeight={600}
                    margin="0 0 16px 0"
                    customStyle={titleFontSize}
                >
                    {title}
                </Typography>
                <Typography
                    component={'p'}
                    fontSize="16px"
                    fontWeight={400}
                    margin="0 0 24px 0"
                >
                    {mainText}
                </Typography>
                <Typography
                    component={'h2'}
                    fontSize="16px"
                    fontWeight={700}
                    margin="0 0 40px 0"
                >
                    {bottomText}
                </Typography>

                <Button onClick={onClick}>
                    {buttonText}
                </Button>
            </LeftSection>
            <RightSection>
                <Icon src={src} />
            </RightSection>
        </Wrapper>
    )
}