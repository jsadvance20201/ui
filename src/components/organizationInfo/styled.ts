import styled from 'styled-components';

export const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: row wrap;
    flex-flow: row wrap;
    justify-content: space-between;
`;

export const LeftSection = styled.div`
    flex: 1 0 100%;
    order: 1;

    @media (min-width: 810px) {
        flex: 2 0 45%;
        order 0;
        width: 600px;
    }
`;

export const RightSection = styled.div`
    flex: 1 0 100%;
    display: flex;
    order: 0;
    justify-content: center;

    @media (min-width: 810px) {
        flex: 1 0 45%;
        order: 1;
        justify-content: flex-end;
    }
`;

export const Icon = styled.img`
    min-width: 30%;
    max-width: 60%;

    @media (min-width: 810px) {
        min-width: 50%;
        max-width: 70%;
    }
`;