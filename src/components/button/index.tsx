import * as React from 'react';

import { ButtonWrapper } from './styled';

/**
 * @prop {string} buttonType Applies the styles of chosen button type. Optional parameter (by default: 'primary')
 */
interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
    buttonType?: 'primary' | 'secondary' | 'disabled';
}

// Кнопка.
export const Button: React.FunctionComponent<ButtonProps> = ({ children, buttonType, ...rest }) => (
    <ButtonWrapper {...rest} buttonType={buttonType}>
        {children}
    </ButtonWrapper>
);

Button.defaultProps = {
    buttonType: 'primary'
}
