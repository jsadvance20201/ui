import * as React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { Button } from './';

describe('Button component', () => {
    it('Отрисовываетсяи без параметров', () => {
        const tree = renderer.create(<Button />);
        expect(tree).toMatchSnapshot();
    });

    it('Отрисовываетсяи с children', () => {
        const tree = renderer.create(<Button>Кнопка</Button>);
        expect(tree).toMatchSnapshot();
    });

    it('Отрисовывается заблокированная кнопка', () => {
        const tree = renderer.create(<Button disabled={true} >Кнопка</Button>);
        expect(tree).toMatchSnapshot();
    });

    it('Можно пробросить свой className', () => {
        const tree = renderer.create(<Button className="test-class-name">Кнопка</Button>);
        expect(tree).toMatchSnapshot();
    });

    it('Запускает обработчик при нажатии', () => {
        const jestCallback = jest.fn();

        const button = shallow(
            <Button onClick={jestCallback}/>
        );

        button.simulate('click');

        expect(jestCallback.mock.calls.length).toBe(1);
    });
});
