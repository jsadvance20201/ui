import * as React from 'react';
import { storiesOf } from '@storybook/react';

import Error from '.';

storiesOf('Error', module)
    .add('Ошибка', () => (
        <Error
            type="error"
        />
    ))
    .add('404', () => (
        <Error
            type="404"
        />
    ))
    .add('Меняем ссылку', () => (
        <Error link="http://89.223.92.18:8080/" linkText="Проверь дженкинс" />
    ))


