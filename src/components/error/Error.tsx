import React from 'react';

import { Error, Error404 } from '../../assets';

import { Wrapper, ErrorHeader, ErrorBody, Link } from './styled';

/*
* Вариант ошибки.
*
* error - общая ошибка.
* 404 - ошибка 404.
*/
type type = 'error' | '404';

interface ErrorProps {
    type?: type;
    link?: string;
    linkText?: string;
}

interface ErrorDataItem {
    icon: React.FC;
    head: string;
    body?: string;
}

const data: { [key: string]: ErrorDataItem } = {
    error: {
        icon: Error,
        head: 'Что-то пошло не так',
        body: 'Мы уже работаем над этим',
    },
    404: {
        icon: Error404,
        head: 'Страница не найдена',
    }
};

export default function ({
    type = 'error',
    link = '/',
    linkText = 'Перейти на главную →'
}: ErrorProps) {
    const errorData = data[type];
    const Icon = errorData.icon;

    return (
        <Wrapper>
            <Icon />
            <ErrorHeader>{errorData.head}</ErrorHeader>
            {errorData.body && <ErrorBody>{errorData.body}</ErrorBody>}
            <Link href={link}>{linkText}</Link>
        </Wrapper>
    );
}
