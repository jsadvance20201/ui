import * as React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import Error from './';

describe('Error component', () => {
    it('Снэпшотим варианты', () => {
        expect(mount(<Error />)).toMatchSnapshot();
        expect(mount(<Error type="404" />)).toMatchSnapshot();
        expect(mount(<Error link="/main" type="error" linkText="Вперед..." />)).toMatchSnapshot();
    });
});
