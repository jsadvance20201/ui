import styled from 'styled-components';

export const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    height: calc(100vh - 208px);

    svg {
        max-width: 100%;
    }
`;

export const ErrorHeader = styled.h3`
    margin-bottom: 4px;
`;

export const ErrorBody = styled.p`
    margin-bottom: 28px;
`;

export const Link = styled.a`
    font-family: Montserrat;
    font-size: 16px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #003eff;
    margin-top: 4px;
`;
