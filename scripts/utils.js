const globSync = require('glob').sync

exports.globSvg = (iconSetName) => globSync(`${iconSetName}/**/*.svg`)
